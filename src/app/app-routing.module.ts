import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BooksLibraryComponent } from './books-library/books-library.component';


const routes: Routes = [
  { path: '', redirectTo: '/book-library', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'book-library', component: BooksLibraryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
