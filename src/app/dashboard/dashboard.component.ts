import { Component, OnInit } from '@angular/core';
import { MessageService } from '../core/message.service';
import { MyHttpService } from '../my-http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    public msgService: MessageService,
    private httpService: MyHttpService) { }

  onClick(msg: string) {
    this.msgService.add(msg);
  }

  onClick2(value: number) {
    this.httpService.getMsgFromServer(value)
      .subscribe(
        erg => this.msgService.add(erg),
        err => this.msgService.add(err.message)
      );
  }

  ngOnInit(): void {
  }

}
