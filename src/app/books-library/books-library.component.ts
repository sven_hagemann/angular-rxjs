import { Component, OnInit } from '@angular/core';
import { MyHttpService } from '../my-http.service';
import { MessageService } from '../core/message.service';

@Component({
  selector: 'app-books-library',
  templateUrl: './books-library.component.html',
  styleUrls: ['./books-library.component.scss']
})
export class BooksLibraryComponent implements OnInit {

  constructor(
    private http: MyHttpService,
    private msgService: MessageService) { }

  ngOnInit(): void {
  }

  getAllBooks() {
    // this.http.getAllBooks().subscribe(books => console.log(books));
    this.http.getAllBooks().subscribe(books => this.msgService.add(books[1].title));
  }

}
