import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { MessageService } from './core/message.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MyHttpService {
  private serverUrl = '';
  // httpOptions = {
  //   headers: new HttpHeaders().set('Content-Type', 'application/json'),
  //   httpParams: new HttpParams().set('operation', 'square')
  // };

  // httpHeaders = new HttpHeaders().set('Accept', 'application/json');
  // httpParams = new HttpParams().set('operation', 'square');

  constructor(
    private http: HttpClient,
    private msgService: MessageService) { }

  private log(msg: string) {
    this.msgService.add(`HttpService: ${msg}`);
  }

  getMsgFromServer(value: number): Observable<any> {
    return this.http.post(`${this.serverUrl}/calculate/${value}`,
                            {operation: 'square'},
                            {headers: new HttpHeaders().set('Content-Type', 'application/json')}
                            ).pipe(tap(res => console.log(res)));
  }

  getAllBooks(){
    console.log('getAllBooks...');
    
    return this.http.get(`${this.serverUrl}/books`);
  }
}
